package com.revolver.eventbus

interface EventListener {

    fun onEvent(event: Event)
}