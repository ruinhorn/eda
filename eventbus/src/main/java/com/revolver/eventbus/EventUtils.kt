package com.revolver.eventbus

import io.reactivex.disposables.CompositeDisposable

fun CompositeDisposable.listenEvents(listener: (Event) -> Unit) {
    add(RxBus.listenEvents(Event::class.java).subscribe(listener))
}

fun CompositeDisposable.listenNavigation(listener: (NavigationEvent) -> Unit) {
    add(RxBus.listenNavigations(NavigationEvent::class.java).subscribe(listener))
}