package com.revolver.eventbus

import com.example.core.model.Task
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import io.reactivex.subjects.Subject

object RxBus : Syncable {

    private var eventEmitter: Subject<Event>? = null

    private var navigatorEmitter: ReplaySubject<NavigationEvent>? = null

    override fun onCreate() {
        eventEmitter = PublishSubject.create<Event>()
        navigatorEmitter = ReplaySubject.createWithSize<NavigationEvent>(1)
    }

    internal fun emit(event: Event) {
        eventEmitter!!.onNext(event)
    }

    internal fun navigate(event: NavigationEvent) {
        navigatorEmitter!!.onNext(event)
    }

    fun listenEvents(eventClass: Class<Event>) =
        eventEmitter!!.ofType(eventClass)

    fun listenNavigations(navigationEvent: Class<NavigationEvent>) =
        navigatorEmitter!!.ofType(navigationEvent)

    override fun onDestroy() {
        eventEmitter = null
        navigatorEmitter = null
    }
}

sealed class Event {

    fun publish() = RxBus.emit(this)

    data class TaskListEvent(val taskList: List<Task>) : Event()
    data class TaskSaveEvent(val task: Task) : Event()
    data class ShowErrorEvent(val text: String? = null): Event()
    object TaskSaveSuccessEvent : Event()
}

sealed class NavigationEvent {

    fun publish() = RxBus.navigate(this)

    object StartTaskList : NavigationEvent()
    object PopFragment : NavigationEvent()
    data class StartAddEditTaskFragment(val task: Task? = null): NavigationEvent()
    data class StartTaskDetailFragment(val task: Task): NavigationEvent()
}



