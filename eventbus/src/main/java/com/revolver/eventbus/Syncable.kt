package com.revolver.eventbus

interface Syncable {

    fun onCreate()

    fun onDestroy()
}