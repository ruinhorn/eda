package com.revolver.eventbus

import io.reactivex.disposables.CompositeDisposable

abstract class BaseProcessor : Syncable {

    protected val disposable = CompositeDisposable()

    abstract fun onEvent(event: Event)

    override fun onCreate() {
        disposable.listenEvents(::onEvent)
    }

    override fun onDestroy() {
        disposable.clear()
    }
}