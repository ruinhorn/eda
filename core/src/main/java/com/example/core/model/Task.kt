package com.example.core.model

import java.util.UUID

data class Task(
    var title: String? = null,
    var description: String? = null,
    val id: String = UUID.randomUUID().toString(),
    var isCompleted: Boolean = false
) {
    val titleForList: String?
        get() = if (!title.isNullOrEmpty()) title else description

    val isActive
        get() = !isCompleted

    val isEmpty
        get() = title.isNullOrEmpty() && description.isNullOrEmpty()
}