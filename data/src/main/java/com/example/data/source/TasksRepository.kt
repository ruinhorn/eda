/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.data.source


import com.example.data.source.local.TasksLocalDataSource
import com.example.data.source.model.TaskDatabaseModel
import io.reactivex.Observable

class TasksRepository(
    val tasksLocalDataSource: TasksLocalDataSource
) : TasksDataSource {

    override fun clearCompletedTasks() {
    }

    override fun getTasks(): Observable<List<TaskDatabaseModel>> {
        return tasksLocalDataSource.getTasks()
    }

    override fun saveTask(task: TaskDatabaseModel) =
        tasksLocalDataSource.saveTask(task)

    override fun completeTask(task: TaskDatabaseModel) =
        tasksLocalDataSource.completeTask(task)

    override fun completeTask(taskId: String) {
    }

    override fun activateTask(task: TaskDatabaseModel) {
    }

    override fun activateTask(taskId: String) {
    }

    override fun getTask(taskId: String) =
        tasksLocalDataSource.getTask(taskId)

    override fun deleteAllTasks() {
    }

    override fun deleteTask(taskId: String) {
        tasksLocalDataSource.deleteTask(taskId)
    }

    override fun refreshTasks() {
    }
}
