package com.example.data.source

import androidx.room.Room
import com.example.data.source.local.TasksLocalDataSource
import com.example.data.source.local.ToDoDatabase
import org.koin.dsl.module.module


val dataModule = module {

    single {
        Room.databaseBuilder(
            get(),
            ToDoDatabase::class.java, "Tasks"
        )
            .build()
    }

    single { get<ToDoDatabase>().taskDao() }

    single { TasksLocalDataSource(get()) }

    single { TasksRepository(get()) }
}
