/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.data.source.local

import com.example.data.source.TasksDataSource
import com.example.data.source.model.TaskDatabaseModel
import io.reactivex.Observable


/**
 * Concrete implementation of a data source as a db.
 */
class TasksLocalDataSource(private val tasksDao: TasksDao) : TasksDataSource {

    override fun getTask(taskId: String) = tasksDao.getTaskById(taskId)

    override fun saveTask(task: TaskDatabaseModel) =
        Observable.fromCallable { tasksDao.insertTask(task) }

    override fun completeTask(task: TaskDatabaseModel) {
    }

    override fun completeTask(taskId: String) {
    }

    override fun activateTask(task: TaskDatabaseModel) {
    }

    override fun activateTask(taskId: String) {
    }

    override fun clearCompletedTasks() {
    }

    override fun refreshTasks() {
    }

    override fun deleteAllTasks() {
    }

    override fun deleteTask(taskId: String) {
    }

    override fun getTasks(): Observable<List<TaskDatabaseModel>> = tasksDao.getTasks()
}


