package com.example.data.source

import android.annotation.SuppressLint
import com.example.core.model.Task
import com.example.data.source.model.TaskDatabaseModel
import com.revolver.eventbus.BaseProcessor
import com.revolver.eventbus.Event
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.inject

object TaskProcessor : BaseProcessor(), KoinComponent {

    private val taskRepository: TasksRepository by inject()

    init {
        loadKoinModules(listOf(dataModule))
    }

    override fun onCreate() {
        super.onCreate()
        disposable.add(
            taskRepository.getTasks()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Event.TaskListEvent(it.toCore()).publish()
                }
        )
    }

    override fun onEvent(event: Event) {
        when (event) {
            is Event.TaskSaveEvent -> saveTask(event.task)
        }
    }

    @SuppressLint("CheckResult")
    private fun saveTask(task: Task) {
            taskRepository
                .saveTask(task.toDatabaseModel())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    Event.TaskSaveSuccessEvent.publish()
                }
        }
    }

private fun Task.toDatabaseModel() = TaskDatabaseModel(
    id = this.id,
    title = this.title ?: "",
    description = this.description ?: "",
    isCompleted = this.isCompleted
)

private fun TaskDatabaseModel.toCoreModel() = Task(
    id = this.id,
    title = this.title,
    description = this.description,
    isCompleted = this.isCompleted
)

private fun List<TaskDatabaseModel>.toCore() = map { it.toCoreModel() }
