package com.revolver.eventdriven.presentation.tasks

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.example.core.model.Task
import com.revolver.eventbus.Event
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventdriven.R
import com.revolver.eventdriven.presentation.base.BaseEventFragment
import com.revolver.eventdriven.util.setUp
import kotlinx.android.synthetic.main.tasks_frag.drawer_layout
import kotlinx.android.synthetic.main.tasks_frag.fab_add_task
import kotlinx.android.synthetic.main.tasks_frag.toolbar
import kotlinx.android.synthetic.main.tasks_layout.filteringLabel
import kotlinx.android.synthetic.main.tasks_layout.noTasks
import kotlinx.android.synthetic.main.tasks_layout.noTasksAdd
import kotlinx.android.synthetic.main.tasks_layout.refresh_layout
import kotlinx.android.synthetic.main.tasks_layout.tasks_list

class TasksFragment : BaseEventFragment<NavigationEvent.StartTaskList>() {
    override val layoutRes: Int = R.layout.tasks_frag

    //TODO events, filter label, loading progress, no tasks label

    private val taskAdapter = TaskAdapter(emptyList())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (taskAdapter.taskList.isNotEmpty()) {
            hideNoTodo()
        } else {
            showNoTodo()
        }
        refresh_layout.apply {
            setColorSchemeColors(
                ContextCompat.getColor(requireContext(), R.color.colorPrimary),
                ContextCompat.getColor(requireContext(), R.color.colorAccent),
                ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
            )
            // Set the scrolling view in the custom SwipeRefreshLayout.
            scrollUpChild = tasks_list
            setOnRefreshListener {
                //TODO refresh event
            }
        }
        initToolbar()

        tasks_list.adapter = taskAdapter
        fab_add_task.setOnClickListener { onAddTaskClick() }
        noTasksAdd.setOnClickListener { onAddTaskClick() }
        filteringLabel.text = resources.getString(R.string.label_all)
        //TODO for test
    }

    override fun onEvent(event: Event) {
        when (event) {
            is Event.TaskListEvent -> {
                showTaskList(event.taskList)
            }
        }
    }

    private fun initToolbar() {
        toolbar.setUp(
            { drawer_layout.openDrawer(GravityCompat.START) },
            menu = R.menu.tasks_fragment_menu,
            menuItemClick = ::onMenuItemClick
        )
    }

    private fun showTaskList(taskList: List<Task>) {
        if (taskList.isNotEmpty()) {
            taskAdapter.taskList = taskList
            hideNoTodo()
        } else {
            showNoTodo()
        }
    }

    private fun showNoTodo() {
        noTasks.visibility = View.VISIBLE
        tasks_list.visibility = View.GONE
    }

    private fun hideNoTodo() {
        noTasks.visibility = View.GONE
        tasks_list.visibility = View.VISIBLE
    }

    private fun onMenuItemClick(id: Int): Boolean {
        when (id) {
            R.id.menu_clear -> {
            }//TODO clear completed
            R.id.menu_filter -> showFilteringPopUpMenu()
            R.id.menu_refresh -> {
            }//TODO refresh
        }
        return true
    }

    fun showFilteringPopUpMenu() {
        //TODO
        val activity = activity ?: return
        val context = context ?: return
        PopupMenu(context, activity.findViewById(R.id.menu_filter)).apply {
            menuInflater.inflate(R.menu.filter_tasks, menu)
            setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.active -> {
                    }
                    R.id.completed -> {
                    }
                    else -> {
                    }
                }
                true
            }
            show()
        }
    }

    private fun onAddTaskClick() {
        //TODO add task event
        NavigationEvent.StartAddEditTaskFragment().publish()
    }
}