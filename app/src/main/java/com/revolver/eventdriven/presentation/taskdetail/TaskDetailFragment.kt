package com.revolver.eventdriven.presentation.taskdetail

import android.os.Bundle
import android.view.View
import com.example.core.model.Task
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventdriven.R
import com.revolver.eventdriven.presentation.base.BaseFragment
import com.revolver.eventdriven.util.setUp
import kotlinx.android.synthetic.main.taskdetail_frag.fab_edit
import kotlinx.android.synthetic.main.taskdetail_frag.task_detail_complete
import kotlinx.android.synthetic.main.taskdetail_frag.task_detail_description
import kotlinx.android.synthetic.main.taskdetail_frag.task_detail_title
import kotlinx.android.synthetic.main.taskdetail_frag.toolbar

class TaskDetailFragment : BaseFragment<NavigationEvent.StartTaskDetailFragment>() {
    override val layoutRes: Int = R.layout.taskdetail_frag

    private val task: Task
        get() = payload?.task ?: throw NullPointerException("Task must not be null")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.setUp(
            activity!!::onBackPressed,
            menu = R.menu.taskdetail_fragment_menu,
            menuItemClick = onMenuItemClick()
        )

        task_detail_complete.isChecked = task.isCompleted
        task_detail_title.text = task.title
        task_detail_description.text = task.description

        task_detail_complete.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                //TODO complete task event
            } else {
                //TODO active task event
            }
        }
        fab_edit.setOnClickListener {
            NavigationEvent.StartAddEditTaskFragment(task).publish()
        }
    }

    private fun onMenuItemClick(): (Int) -> Boolean {
        return {
            if (it == R.id.menu_delete) {
                //TODO delete event
            }
            true
        }
    }
}