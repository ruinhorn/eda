package com.revolver.eventdriven.presentation.base

import com.revolver.eventbus.EventListener
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventbus.listenEvents

abstract class BaseEventFragment<T : NavigationEvent> : BaseFragment<T>(), EventListener {

    override fun onStart() {
        super.onStart()
        disposable.listenEvents(::onEvent)
    }
}