package com.revolver.eventdriven.presentation.tasks

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.core.model.Task
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventdriven.R
import com.revolver.eventdriven.util.inflate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.task_item.complete
import kotlinx.android.synthetic.main.task_item.title

class TaskAdapter(taskList: List<Task>) : RecyclerView.Adapter<TaskAdapter.TaskViewHolder>() {

    var taskList: List<Task> = taskList
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(parent.inflate(R.layout.task_item))
    }

    override fun getItemCount() = taskList.size

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(taskList[position])
    }

    class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {

        override val containerView: View? = itemView

        fun bind(task: Task) {
            title.text = task.titleForList
            complete.isChecked = task.isCompleted
            val rowViewBackground = if (task.isCompleted) R.drawable.list_completed_touch_feedback
            else R.drawable.touch_feedback
            itemView.setBackgroundResource(rowViewBackground)

            complete.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    //TODO complete task event
                } else {
                    //TODO active task event
                }
            }
            itemView.setOnClickListener {
                NavigationEvent.StartTaskDetailFragment(task).publish()
            }
        }
    }
}