package com.revolver.eventdriven.presentation.addedittask

import android.os.Bundle
import android.view.View
import com.example.core.model.Task
import com.revolver.eventbus.Event
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventdriven.R
import com.revolver.eventdriven.presentation.base.BaseEventFragment
import com.revolver.eventdriven.util.hideKeyboard
import com.revolver.eventdriven.util.setUp
import com.revolver.eventdriven.util.showKeyboard
import com.revolver.eventdriven.util.showSnackbar
import kotlinx.android.synthetic.main.add_edit_task_frag.add_task_description
import kotlinx.android.synthetic.main.add_edit_task_frag.add_task_title
import kotlinx.android.synthetic.main.add_edit_task_frag.fab_done
import kotlinx.android.synthetic.main.add_edit_task_frag.root
import kotlinx.android.synthetic.main.add_edit_task_frag.toolbar

class AddEditTaskFragment : BaseEventFragment<NavigationEvent.StartAddEditTaskFragment>() {

    override val layoutRes: Int = R.layout.add_edit_task_frag

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        add_task_title.requestFocus()
        showKeyboard(add_task_title)

        toolbar.setUp({ activity?.onBackPressed() }, payload?.let { R.string.edit_task })

        payload?.task?.let {
            add_task_title.setText(it.title)
            add_task_description.setText(it.description)
        }

        fab_done.setOnClickListener {
            val task = getTask()
            if (task.title.isNullOrEmpty()) {
                root.showSnackbar(getString(R.string.empty_task_message))
            } else {
                Event.TaskSaveEvent(getTask()).publish()
            }
        }
    }

    override fun onEvent(event: Event) {
        when (event) {
            is Event.TaskSaveSuccessEvent -> {
                hideKeyboard()
                NavigationEvent.PopFragment.publish()
            }
        }
    }

    private fun getTask() =
        (payload?.task ?: Task()).apply {
            title = add_task_title.text.toString()
            description = add_task_description.text.toString()
        }
}