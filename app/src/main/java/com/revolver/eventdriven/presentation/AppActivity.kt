package com.revolver.eventdriven.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.data.source.TaskProcessor
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventbus.RxBus
import com.revolver.eventbus.Syncable
import com.revolver.eventbus.listenNavigation
import com.revolver.eventdriven.R
import com.revolver.eventdriven.presentation.addedittask.AddEditTaskFragment
import com.revolver.eventdriven.presentation.taskdetail.TaskDetailFragment
import com.revolver.eventdriven.presentation.tasks.TasksFragment
import io.reactivex.disposables.CompositeDisposable

class AppActivity : AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()
    private val syncedModules = listOf(RxBus, TaskProcessor)

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        syncedModules.onCreate()
        setContentView(R.layout.activity_main)
        compositeDisposable.listenNavigation(::onNavigation)
        NavigationEvent.StartTaskList.publish()
    }

    private fun onNavigation(event: NavigationEvent) {
        when (event) {
            is NavigationEvent.StartTaskList -> TasksFragment().replace()
            is NavigationEvent.StartAddEditTaskFragment -> AddEditTaskFragment().replace(true)
            is NavigationEvent.StartTaskDetailFragment -> TaskDetailFragment().replace(true)
            is NavigationEvent.PopFragment -> {
                supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        syncedModules.onDestroy()
        compositeDisposable.dispose()
    }

    private fun Fragment.replace(addToBackStack: Boolean = false) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.root, this, this::class.java.simpleName)
            .also {
                if (addToBackStack) {
                    it.addToBackStack(null)
                }
            }
            .commit()
    }

    private fun Fragment.add(addToBackStack: Boolean = false) {
        supportFragmentManager.beginTransaction()
            .add(R.id.root, this, this::class.java.simpleName)
            .also {
                if (addToBackStack) {
                    it.addToBackStack(null)
                }
            }
            .commit()
    }

    private fun List<Syncable>.onCreate() {
        forEach { it.onCreate() }
    }

    private fun List<Syncable>.onDestroy() {
        forEach { it.onDestroy() }
    }
}

