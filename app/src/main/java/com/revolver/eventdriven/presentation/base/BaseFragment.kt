package com.revolver.eventdriven.presentation.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.revolver.eventbus.NavigationEvent
import com.revolver.eventbus.RxBus
import io.reactivex.disposables.CompositeDisposable

abstract class BaseFragment<T : NavigationEvent> : Fragment() {

    protected abstract val layoutRes: Int

    protected var payload: T? = null
    protected val disposable = CompositeDisposable()

    @Suppress("UNCHECKED_CAST")
    @CallSuper
    override fun onAttach(context: Context) {
        super.onAttach(context)
        RxBus.listenNavigations(NavigationEvent::class.java)
            .subscribe {
                payload = it as T
            }.dispose()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutRes, container, false)
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }
}