package com.revolver.eventdriven.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import com.google.android.material.snackbar.Snackbar
import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment


internal fun ViewGroup.inflate(
    @LayoutRes layoutRes: Int, viewGroup: ViewGroup = this,
    attachToRoot: Boolean = false
) =
    LayoutInflater.from(context).inflate(layoutRes, viewGroup, attachToRoot)

internal fun Toolbar.setUp(
    navigationAction: () -> Unit,
    @StringRes title: Int? = null,
    @MenuRes menu: Int? = null,
    menuItemClick: ((Int) -> Boolean)? = null
) {
    setNavigationOnClickListener { navigationAction() }
    title?.let { setTitle(it) }
    menu?.let { menu ->
        inflateMenu(menu)
        menuItemClick?.let { menuItemClick ->
            setOnMenuItemClickListener {
                return@setOnMenuItemClickListener menuItemClick(it.itemId)
            }
        }
    }
}

internal fun View.showSnackbar(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(this, message, duration).show()
}

internal fun Fragment.hideKeyboard() {
    activity?.let {
        val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        (it.currentFocus ?: view)?.let {
            imm.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }
}

internal fun Fragment.showKeyboard(viewFocused: View) {
    activity?.let {
        val imm = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(viewFocused, 0)
    }
}